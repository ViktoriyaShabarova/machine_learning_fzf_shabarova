#define ANNDLL_EXPORTS

#include "MyNN.h"
#include <iostream>
using namespace std;

MyNN::MyNN(vector<size_t> conf, ANeuralNetwork::ActivationType aType, float scale)
{
	this->configuration = conf;
	this->activation_type = aType;
	this->scale = scale;
	if (conf.empty())
		return;
}


MyNN::~MyNN()
{

}

string MyNN::GetType()
{
	return ("My NN");
}

vector<float> MyNN::Predict(std::vector<float> & input)
{
	if (!is_trained || configuration.empty() || configuration[0] != input.size())
		cout << L"Pustota" << endl;


	vector<float> prev_out;
	vector<float> out;
	prev_out = input;

	for (size_t layer_idx = 0; layer_idx < configuration.size() - 1; layer_idx++)
	{
		out.resize(configuration[layer_idx + 1], 0);
		for (size_t to_idx = 0; to_idx < configuration[layer_idx + 1]; to_idx++)
		{
			for (size_t from_idx = 0; from_idx < configuration[layer_idx]; from_idx++)
			{
				out[to_idx] += weights[layer_idx][from_idx][to_idx] * prev_out[from_idx];
			}
			out[to_idx] = Activation(out[to_idx]);
		}
		prev_out = out;
	}
	return prev_out;
}
ANNDLL_API shared_ptr<ANN::ANeuralNetwork> ANN::CreateNeuralNetwork(vector<size_t>& conf, ANN::ANeuralNetwork::ActivationType aType, float scale)
{
	return make_shared < MyNN>(conf, aType, scale);
}

ANNDLL_API float ANN::BackPropTraining(
	std::shared_ptr<ANN::ANeuralNetwork> ann,
	std::vector<std::vector<float>> & inputs,
	std::vector<std::vector<float>> & outputs,
	int maxIters,
	float eps,
	float speed,
	bool std_dump)
{
	ann->RandomInit();
	if (inputs.size() != outputs.size())
		throw runtime_error("314");

	float error = 0;
	int currentIter = 0;
	do
	{
		error = 0;
		for (size_t counter_idx = 0; counter_idx < inputs.size(); counter_idx++)
		{
			error += BackPropTrainingIteration(ann, inputs[counter_idx], outputs[counter_idx], speed);
		}
		currentIter++;
		error = sqrt(error);

		if (std_dump && currentIter % 100 == 0)
		{
			cout << "Iteration " << currentIter << "\tError:" << error << endl;
		}
		if (error < eps)
		{
			ann->is_trained = true;
		}
	} while (error > eps && currentIter <= maxIters);
	return error;
}
ANNDLL_API float ANN::BackPropTrainingIteration(
	std::shared_ptr<ANN::ANeuralNetwork> ann,
	const std::vector<float>& input,
	const std::vector<float>& output,
	float speed
	)
{
	float error(0);
	vector<vector<float>> outs(ann->configuration.size());

	outs[0] = input;
	for (size_t layer_idx = 0; layer_idx < ann->configuration.size() - 1; layer_idx++)
	{
		outs[layer_idx + 1].resize(ann->configuration[layer_idx + 1]);
		for (size_t to_idx = 0; to_idx < ann->configuration[layer_idx + 1]; to_idx++)
		{
			outs[layer_idx + 1][to_idx] = 0;
			for (size_t from_idx = 0; from_idx < ann->configuration[layer_idx]; from_idx++)
			{
				outs[layer_idx + 1][to_idx] += outs[layer_idx][from_idx] * ann->weights[layer_idx][from_idx][to_idx];
			}
			outs[layer_idx + 1][to_idx] = ann->Activation(outs[layer_idx + 1][to_idx]);
		}
	}
	//������ ���� �� ������ 
	vector<vector<float>> sigma(ann->configuration.size());
	vector<vector<vector<float>>> dw(ann->configuration.size() - 1);
	sigma.back().resize(outs.back().size());

	for (size_t out_idx = 0; out_idx < output.size(); out_idx++)
	{
		sigma.back()[out_idx] = (output[out_idx] - outs.back()[out_idx]) * ann->ActivationDerivative(outs.back()[out_idx]);
		error += (output[out_idx] - outs.back()[out_idx]) * (output[out_idx] - outs.back()[out_idx]);
	}
	//��� ����� ���� 
	for (size_t layer_idx = ann->configuration.size() - 2; layer_idx + 1 != 0; layer_idx--)
	{
		dw[layer_idx].resize(ann->weights[layer_idx].size());
		sigma[layer_idx].resize(ann->configuration[layer_idx], 0);

		for (size_t from_idx = 0; from_idx < ann->configuration[layer_idx]; from_idx++)
		{
			for (size_t to_idx = 0; to_idx < ann->configuration[layer_idx + 1]; to_idx++)
			{
				sigma[layer_idx][from_idx] += sigma[layer_idx + 1][to_idx] * ann->weights[layer_idx][from_idx][to_idx];
			}
			sigma[layer_idx][from_idx] *= ann->ActivationDerivative(outs[layer_idx][from_idx]);
			dw[layer_idx][from_idx].resize(ann->weights[layer_idx][from_idx].size());

			for (size_t To_idx = 0; To_idx < ann->configuration[layer_idx + 1]; To_idx++)
			{
				dw[layer_idx][from_idx][To_idx] = speed *sigma[layer_idx + 1][To_idx] * outs[layer_idx][from_idx];
			}
		}
	}
	//����������� �����
	for (size_t layer_idx = 0; layer_idx < ann->weights.size(); layer_idx++)
	{
		for (size_t from_idx = 0; from_idx < ann->weights[layer_idx].size(); from_idx++)
		{
			for (size_t to_idx = 0; to_idx < ann->weights[layer_idx][from_idx].size(); to_idx++)
			{
				ann->weights[layer_idx][from_idx][to_idx] += dw[layer_idx][from_idx][to_idx];
			}
		}
	}
	return error;
}
