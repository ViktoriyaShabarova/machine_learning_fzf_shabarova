#pragma once
#include "ANN.h"
class MyNN :
	public ANN ::ANeuralNetwork 
{
public:

	virtual std::string GetType();
	virtual std::vector <float> Predict(std::vector<float>&input);
	
	MyNN(std::vector<size_t> conf, ANeuralNetwork::ActivationType aType, float scale);
	~MyNN();


};

